package com.lsw.gamemanager.entity;

import com.lsw.gamemanager.interfaces.CommonModelBuilder;
import com.lsw.gamemanager.model.GameUserRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED )
public class GameUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15, unique = true)
    private String username;
    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 13, unique = true)
    private String phone;

    @Column(columnDefinition = "TEXT")
    private String memo;

    private GameUser(GameUserBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.birthday = builder.birthday;
        this.name = builder.name;
        this.phone = builder.phone;
    }
    public static class GameUserBuilder implements CommonModelBuilder<GameUser> {
        private final String username;
        private final String password;
        private final LocalDate birthday;
        private final String name;
        private final String phone;

        public GameUserBuilder(GameUserRequest request){
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.birthday = request.getBirthday();
            this.name = request.getName();
            this.phone = request.getPhone();
        }

        @Override
        public GameUser build() {
            return new GameUser(this);
        }
    }

}
