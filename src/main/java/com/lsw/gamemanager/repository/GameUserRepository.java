package com.lsw.gamemanager.repository;

import com.lsw.gamemanager.entity.GameUser;
import org.hibernate.cfg.JPAIndexHolder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameUserRepository extends JpaRepository<GameUser,Long> {

    long countByUsername()
}
