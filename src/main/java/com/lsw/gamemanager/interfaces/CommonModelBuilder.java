package com.lsw.gamemanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
